class Farm < ApplicationRecord
  belongs_to :room
  has_many :plots, dependent: :destroy
  after_commit { broadcast_update_to self.room }
end
