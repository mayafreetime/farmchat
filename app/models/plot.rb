class Plot < ApplicationRecord
  belongs_to :farm, touch: true
  has_one :plant, dependent: :destroy
end
