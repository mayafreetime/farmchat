# frozen_string_literal: true

# a single instance of a plant type, it will track it's growth stage and resources towards the next stage
class Plant < ApplicationRecord
  belongs_to :plant_type
  belongs_to :plot, touch: true

  def current_ascii_image
    plant_type.stages[growth_stage].ascii_image
  end

  def advance_stage
    if growth_stage < plant_type.stages.size - 1
      update(growth_stage: growth_stage + 1)
    else
      destroy
    end
  end
end
