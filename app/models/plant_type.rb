class PlantType < ApplicationRecord
  # this goes in fixtures later
  LETTUCE = {
    name: 'Lettuce',
    info: 'Any of several plants in the Asteracea family (just like sunflowers!) characterized by rapid growth of tasty leaves',
    stage_info: [
      {
        name: 'seed',
        ascii_image: '.',
        water_needs: 2,
        energy_needs: 2,
        absorbtion_rate: 1,
      },
      {
        name: 'seedling',
        ascii_image: 'v',
        water_needs: 5,
        energy_needs: 5,
        harvest_yield: 1,
        absorbtion_rate: 1,
      },
      {
        name: 'young',
        ascii_image: 't',
        water_needs: 10,
        energy_needs: 10,
        gather_yield: 1,
        harvest_yield: 2,
        absorbtion_rate: 2,
      },
      {
        name: 'mature',
        ascii_image: 'W',
        water_needs: 4,
        energy_needs: 4,
        gather_yield: 3,
        harvest_yield: 5,
        absorbtion_rate: 2,
      },
      {
        name: 'bolting',
        ascii_image: 'Y',
        water_needs: 5,
        energy_needs: 10,
        harvest_yield: 2,
        seeds: 4,
        absorbtion_rate: 3,
      },
      {
        name: 'dead',
        ascii_image: 'z',
        seeds: 20,
        absorbtion_rate: 0,
      }
    ]
  }

  def stages
    @_stages ||= stage_info.map {|data| PlantStage.new(**data)}
  end
end

class PlantStage
  include ActiveModel::API

  attr_accessor :name, :ascii_image, :image, :gather_yield, :harvest_yield, :seeds, :water_needs, :energy_needs, :absorbtion_rate

  def can_gather?
    !!gather_yield
  end

  def can_harvest?
    !!harvest_yield
  end
end