# frozen_string_literal: true

# the basic controller letting us create, list, or show the chat rooms
class RoomsController < ApplicationController
  def index
    @current_user = current_user
    redirect_to '/signin' unless @current_user
    @rooms = Room.public_rooms
    @users = User.all_except(@current_user)
    @room = Room.new
  end

  def create
    @room = Room.create(name: params['room']['name'])
  end

  def show
    @current_user = current_user
    @single_room = Room.find(params[:id])
    @farm = @single_room&.farm || Farm.new
    @messages = @single_room.messages
    @rooms = Room.public_rooms
    @users = User.all_except(@current_user)
    @room = Room.new
    @message = Message.new

    render 'index'
  end
end
