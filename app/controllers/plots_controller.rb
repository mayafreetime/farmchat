# frozen_string_literal: true

# the basic controller letting us create or change the plot objects on the farm
class PlotsController < ApplicationController
  def create
    @farm = Farm.find(params[:id])
    @farm.plots.create
    # respond to turbo stuff?
    redirect_to room_path(@farm.room)
  end

  def update
    @farm = Farm.find(params[:farm_id])
    @plot = @farm.plots.find(params[:id])
    if @plot.plant
      @plot.plant.advance_stage
    else
      @plot.create_plant(plant_type: PlantType.first, growth_stage: 0)
    end
  end
end
