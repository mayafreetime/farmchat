# frozen_string_literal: true

# the basic controller letting us create or change the farm objects
class FarmsController < ApplicationController
  def create
    @room = Room.find(params[:room_id])
    @room.build_initial_farm
    # respond to turbo stuff?
    redirect_to room_path(@room)
  end
end
