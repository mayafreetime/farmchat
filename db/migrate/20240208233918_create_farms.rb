class CreateFarms < ActiveRecord::Migration[7.1]
  def change
    create_table :farms do |t|
      t.integer :size
      t.references :room, null: false, foreign_key: true

      t.timestamps
    end
  end
end
