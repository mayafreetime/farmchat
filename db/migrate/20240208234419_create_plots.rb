class CreatePlots < ActiveRecord::Migration[7.1]
  def change
    create_table :plots do |t|
      t.references :farm, null: false, foreign_key: true
      t.string :quality

      t.timestamps
    end
  end
end
