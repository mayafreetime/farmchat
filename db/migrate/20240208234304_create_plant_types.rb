class CreatePlantTypes < ActiveRecord::Migration[7.1]
  def change
    create_table :plant_types do |t|
      t.string :name
      t.text :info
      t.jsonb :stage_info

      t.timestamps
    end
  end
end
