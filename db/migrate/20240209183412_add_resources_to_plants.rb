class AddResourcesToPlants < ActiveRecord::Migration[7.1]
  def change
    add_column :plants, :water, :integer
    add_column :plants, :energy, :integer
  end
end
