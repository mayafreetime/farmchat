class CreatePlants < ActiveRecord::Migration[7.1]
  def change
    create_table :plants do |t|
      t.references :plant_type, null: false, foreign_key: true
      t.references :plot, null: false, foreign_key: true
      t.integer :growth_stage

      t.timestamps
    end
  end
end
