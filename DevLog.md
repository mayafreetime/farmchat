# Developer Log

This is the dev log for this toy project. Thoughts, intentions, and problems will be added in dated entries below, with the most recent at the top.

### Feb 23rd, 2024
Hah! Apparently tailwind uses the codebase to generate a kind of safelist or something, of all the styles it needs to be able to load and display, so generating a class it hasn't seen elsewhere means it doesn't have that css available. It seemed to work yesterday because the css was still loaded after hard-coding it once and then going back to generated classes for the elements I was working with. It doesn't even need to be in style, in fact random code comments seem to work just fine. For now I'm adding them to an unused partial, per the suggestion here: https://github.com/rails/tailwindcss-rails/issues/127

### Feb 22nd, 2024
Played around a bit more with tailwind. Had an interesting issue where border-color classes, when set by interpolated ruby on the view (to change color by some data on the model) wouldn't show up until I had rendered an element in that color without any interpolation. I'm not sure why that happened, or why rendering it manually would fix it, but it probably bears further investigation somewhere. Maybe I'll actualy post a stack overflow question if I still can't find it.

I also ripped out the bootstrap css that was being included on the basic layout page, from when I started this app without tailwind. Doing so changed a couple of things a bit, but I added m-auto to the container and replaced the bg-darg and text-light classes with appropriate color classes, and it's almost the same as it was before now. (Just a bit less side margin as an obvious difference.)

I also looked at getting the plot updates set up as turbo links, but I appear to have accidentally made it work already. Probably the link is effectively remote, and the farm update happening because the plot was updated just makes it work. At some point maybe I'll try removing the 'touch' options and see if I can get it working again--right now the entire farm is re-rendered when a plot is updated, and as I let bigger farms happen that might get less optimal.

Next session I'll add some more functionality to things (adding plots, specifying plant or water as a button instead of the weird link, etc) and continue styling stuff for practice.

### Feb 20th, 2024
Tried out tailwind today, since a lot of places are using it and I definitely need *something* to make this toy prettier. I'm not totally sold on it as a preferred way to do css, it feels a lot like doing inline styles by another name, but it is very cool. I couldn't find a max-width for percentages, and learned that it has 'magic' interpretation so you can give it the value you want in '-[value]' format and it will just do it. So this might get more comfortable with a little more playing around, and meanwhile the toy is already a bit prettier. I added some padding and rearranged the layout a bit, so now the chat window is bigger and below the farm, instead of crammed into the side. (That was partly just because getting the user/room list to stay the height I wanted it was annoying, but I probably figured out enough already for that to be alright next time, and I like it better this way anyway.)

I'll start getting some ui interactions for the farms up next, so the toy is functional from the browser instead of just responding to console events. I still don't know how to get a room to show a new farm, I'm missing something in how turbo works, but I'm okay with waiting to figure that out while I play with the rest of it.

### Feb 15th, 2024 (late but initial thoughts)
I should definitely have added a log before this, but I initially thought this would only be an afternoon of playing with some rails boilerplate. I wound up having fun with it and wanting to do more though, and since I work on multiple computers it's hard to have my notes somewhere that makes sense. Hence, a dev log as part of the repo. That's fine though, it'll be a fun journal when I get done with whatever ths is going to be.

So, initial thoughts and direction:

I wanted to learn how to use the new action cable stuff in Rails 7, and there's piles of tutorials around building a chat app, so I started there. Just chatting is boring though, it's better if there's something to *do* while chatting right? My spouse suggested a community farm of some sort, so I headed off in that direction.

Currently there's a normal chat-app setup of rooms and users, and a room can have a farm, which can have multiple plots (places to plant things), and each plot can have a plant in it. Those plants have some data sketched out for what they will be doing (consuming water and sunlight, producing recsources). I'm tempted to go overboard in that direction, with interplay of plants and complicated flowcharts for what happens if you do or don't harvest a plant, but for now I'm going to try to keep it pretty simple. A plant has multiple stages, generally terminating in 'dead', and each stage has resources it can provide and requirements to reach the next stage.

End goal for this toy is open chat rooms where signed up users can participate in the farms by:
* digging new plots
* 'buying' seeds
* planting seeds
* watering plants
* harvesting plants (for 'energy' and/or 'seeds')

The farms and/or the individual users will (eventually) have an energy upkeep requirement, probably mostly for expanding the farms, possibly also as a user action resource (you need to consume some energy to water a plant, for exmaple), but I would supplement any user requirement with weather sometimes watering automatically to keep anyone from 'starving'. We'll see if I feel like taking the toy that far.

I do not currently plan to make this widely available as a hosted app, but the code will stay open-source for anyone who wants to play with it.