# Farm Chat

This project is a toy to learn how to use Rails' action cable coolness, using a framing of a communal farm project to explore the data and possibilities.

#### System Versions ####
This is using Rails 7 with mostly default options (PostgreSQL option though), and is intended to be kept mostly up to date. Current versions are:
* ruby 3.3.0
* Rails 7.1.3

